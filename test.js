const db = require("./models");
const Screen = db.screens;
const ScreenCampaign = db.screenCampaign;

const screenParser = async screens => {
    let {office, floor, room} = screens;
    if (office === 'ALL') {
        return {office: [1, 2, 3]};
    } else if (floor === 'ALL') {
        let floor;
        await Screen.findAll({attributes: ['floor'], where: {office}}).then(floorsArray => {
            floor = floorsArray.map(floorItem => {
                return floorItem.floor;
            });
        });
        return {office, floor}
    } else if (room === 'ALL') {
        let room;
        await Screen.findAll({attributes: ['room'], where: {office, floor}}).then(roomsArray => {
            room = roomsArray.map(roomItem => {
                return roomItem.room;
            });
        });
        return {office, floor, room}
    } else
        return {office, floor, room}
}

first = {
    office: 'ALL'
}
second = {
    office: 2,
    floor: 'ALL'
}

third = {
    office: 1,
    floor: [3, 8],
    room: [14, 4, 5]
}

fourth = {
    office: 1,
    floor: [3, 8],
    room: 'ALL'
}
// let options;
// screenParser(fourth).then(m => {
//     console.log(m);
//     options = m;
//     Screen.findAll({where: options}).then(m => {
//         console.log(JSON.stringify(m));
//     });
// });

// Screen.aggregate('floor', 'DISTINCT', {
//     order: [
//         ['floor', 'ASC'],
//     ],
//     where: {office: 2},
//     plain: false
// }).then(floors => {
//     console.log(floors);
//     floors = floors.map(floor => floor.DISTINCT)
//     floors.unshift('ALL');
//     console.log(floors);
//     // res.send(floors);
// });

const id = +req.params.id;
const campaign = req.body;
const {userId, name, tag, text, startDate, endDate, design, screen} = campaign;
const tagId = await findTagByName(tag);
let options = await screenParser(screen);
const screens = await Screen.findAll({
    where: options
});
createdCampaign.addScreen(screens);

await ScreenCampaign.

await Campaign.update(
    {
        userId,
        name,
        tagId,
        text,
        startDate,
        endDate
    }, {
        where: {id}
    }
)
    .then(num => {
        console.log(num)
        // if (num == 1) {
        //     // const result = await findByIdInDatabase(id);
        //     // res.send('Campaign was updated successfully');
        // } else {
        //no fields for update
        //     res.send({
        //         message: `Cannot update Campaign with id=. Maybe Campaign was not found!`
        //     });
        // }
    })
    .catch(err => {
        res.status(500).send({
            message: "Could not update Campaign with id="
        });
    });

await Design.destroy({
    where: {campaignId: id}
})
    .then(num => {
        if (num == 1) {
            console.log('Designs is deleted successfully')
        } else {
            console.log(`Cannot delete Design with campaignIdd=${id}. Maybe Design was not found!`)
        }
    });

await createDesignsFromObject(design, id);

const result = await findByIdInDatabase(id);

res.send(result);

};



