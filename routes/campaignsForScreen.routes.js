module.exports = (app, passport) => {
    const campaigns = require("../controllers/campaign.controller.js");

    let router = require("express").Router();

    router.get("/", campaigns.findAllByScreenId);

    app.use('/api/v1/screenUser/campaigns', passport.authenticate('screenJWT', { session: false }), router);
};
