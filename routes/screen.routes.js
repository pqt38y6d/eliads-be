module.exports = (app, passport) => {
    const screens = require("../controllers/screen.controller.js");

    var router = require("express").Router();

    router.post("/", screens.create);

    router.post("/delete", screens.delete);

    router.get("/", screens.findAllBySort);

    router.get("/floors", screens.getFloorsByOffice);

    router.get("/rooms", screens.getRoomsByOfficeAndFloor);

    app.use('/api/v1/screens', passport.authenticate('adminJWT', { session: false }), router);
};
