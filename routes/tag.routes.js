module.exports = (app, passport) => {
    const tags = require("../controllers/tag.controller.js");

    var router = require("express").Router();

    router.post("/", tags.create);

    router.get("/", tags.findAll);

    // Retrieve all Tags with sorting
    // router.get("/", tags.findAllBySort);

    // // Retrieve all published Tutorials
    // router.get("/published", tutorials.findAllPublished);
    //
    // // Retrieve a single Tutorial with id
    // router.get("/:id", tutorials.findOne);
    //
    // // Update a Tutorial with id
    // router.put("/:id", tutorials.update);
    //
    // // Delete a Tutorial with id
    // router.delete("/:id", tutorials.delete);
    //
    // // Create a new Tutorial
    // router.delete("/", tutorials.deleteAll);

    app.use('/api/v1/tags', router);
};
