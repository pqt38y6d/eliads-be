module.exports = (app, passport) => {
    const campaigns = require("../controllers/campaign.controller.js");

    var router = require("express").Router();

    router.post("/", campaigns.create);

    router.post("/delete", campaigns.delete);

    router.get("/", campaigns.findAllBySort);

    router.get("/:id", campaigns.findById);

    router.put("/:id", campaigns.update);


    app.use('/api/v1/campaigns', passport.authenticate('adminJWT', { session: false }), router);
};
