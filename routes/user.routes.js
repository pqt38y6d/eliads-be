module.exports = (app, passport) => {
    const users = require("../controllers/user.controller.js");

    var router = require("express").Router();

    router.post("/", users.create);

    router.post("/delete", users.delete);

    // router.get("/", users.findAllBySort);

    app.use('/api/v1/users', router);
};
