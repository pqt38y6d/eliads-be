const db = require("../models");
const passport = require("passport");
const Screen = db.screens;
const Office = db.offices;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const screen = req.body;

    Screen.create(screen)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Tag."
            });
        });
};

exports.findAllBySort = (req, res) => {
    let {page, size, sort, order} = req.query;
    const limit = +size;
    const offset = page * size;

    console.log(req);


    Screen.findAndCountAll({
        limit,
        offset,
        order: [
            [sort, order]
        ]
    })
        .then(data => {
            const {count, rows} = data;
            const screens = rows;
            res.setHeader('X-total-count', count);
            res.send(screens);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {

};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {

};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
    const ids = req.body.ids;

    Screen.destroy({
        where: {id: ids}
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Tutorial was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Tutorial with id=. Maybe Tutorial was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Tutorial with id="
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {

};

// Find all published Tutorials
exports.findAllPublished = (req, res) => {

};

exports.getFloorsByOffice = (req, res) => {
    const office = req.query;
    Screen.aggregate('floor', 'DISTINCT', {
        order: [
            ['floor', 'ASC'],
        ],
        where: office,
        plain: false
    }).then(floors => {
        floors = floors.map(floor => floor.DISTINCT)
        floors.unshift('ALL');
        res.send(floors);
    });
}

exports.getRoomsByOfficeAndFloor = (req, res) => {
    const query = req.query;
    Screen.aggregate('room', 'DISTINCT', {
        order: [
            ['room', 'ASC'],
        ],
        where: query,
        plain: false
    }).then(floors => {
        floors = floors.map(floor => floor.DISTINCT)
        floors.unshift('ALL');
        res.send(floors);
    });
}

