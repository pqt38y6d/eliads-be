const db = require("../models");
const User = db.users;
const Screen = db.screens;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


const secret = 'my secret';


exports.register = async (req, res) => {
    const {email, password, name, role} = req.body;
    await User.findOne({where: {email}})
        .then(user => {
            if (user) {
                let error = 'User with this email is already exists';
                return res.status(400).json(error);
            } else {
                const newUser = {
                    name: name,
                    email: email,
                    password: password,
                    role: role
                };
                bcrypt.genSalt(10, (err, salt) => {
                    if (err) throw err;
                    bcrypt.hash(newUser.password, salt,
                        (err, hash) => {
                            if (err) throw err;
                            newUser.password = hash;
                            User.create(newUser).then(user => res.json(user))
                                .catch(err => res.status(400).json(err));
                        });
                });
            }
        });
};

exports.login = async (req, res) => {
    const {isScreen} = req.body;
    if (isScreen) {
        const {office, floor, room} = req.body;
        await Screen.findOne({where: {office, floor, room}})
            .then(screen => {
                if (!screen) {
                    let error = "No Screen Found";
                    return res.status(404).json(error);
                }
                const payload = {office, floor, room};
                jwt.sign(payload, secret, {expiresIn: '12h'},
                    (err, token) => {
                        if (err) res.status(500)
                            .json({
                                error: "Error signing token",
                                raw: err
                            });
                        res.json({
                            success: true,
                            token: `Bearer ${token}`
                        });
                    });
            });
    } else {
        const {email, password} = req.body;
        await User.findOne({where: {email}})
            .then(user => {
                if (!user) {
                    let error = "No Account Found";
                    return res.status(404).json(error);
                }
                const {id, name, role, email} = user;
                bcrypt.compare(password, user.password)
                    .then(isMatch => {
                        if (isMatch) {
                            const payload = {
                                id,
                                name,
                                role
                            };
                            jwt.sign(payload, secret, {expiresIn: '12h'},
                                (err, token) => {
                                    if (err) res.status(500)
                                        .json({
                                            error: "Error signing token",
                                            raw: err
                                        });

                                    res.json({
                                        success: true,
                                        token: `Bearer ${token}`,
                                        user: {id, name, role, email}
                                    });
                                });
                        } else {
                            let error = "Password is incorrect";
                            res.status(401).json(error);
                        }
                    });
            });
    }
}

