const db = require("../models");
const Screen = db.screens;
const Tag = db.tags;
const User = db.users;
const Campaign = db.campaigns;
const Design = db.designs;
const screenCampaign = db.screenCampaign;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
    if (!req.body.name) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const campaign = req.body;
    const {tag, design, screen} = req.body;

    campaign.tagId = await findTagByName(tag);
    delete campaign.design;
    delete campaign.tag;
    delete campaign.screen;


    Campaign.create(campaign).then(async createdCampaign => {
            let options = await screenParser(screen);
            const screens = await Screen.findAll({
                where: options
            });
            createdCampaign.addScreen(screens);
            const campaignId = createdCampaign.id;
            await createDesignsFromObject(design, campaignId);
            res.send(createdCampaign);
        }
    ).catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the Campaign."
        });
    })
};

exports.findAllBySort = async (req, res) => {
    const {page, size, sort, order} = req.query;
    const limit = +size;
    const offset = page * size;
    const {id, role} = req.user;

    const optionsForSearchCampaignsInDB = {
        limit,
        offset,
        order: [
            [sort, order]
        ]
    };

    if (role === 'admin') {
        optionsForSearchCampaignsInDB.where = {'userId': id}
    }

    const campaignsFromDB = await Campaign.findAndCountAll(optionsForSearchCampaignsInDB).catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while retrieving tutorials."
        });
    });

    const {count, rows} = campaignsFromDB;
    const campaigns = rows;
    const campaignsForResponse = [];

    for (let campaign of campaigns) {
        const {id, name, text, startDate, endDate, tagId, userId} = campaign;
        const designs = await campaign.getDesigns();
        const user = await User.findByPk(userId);
        const tag = await Tag.findByPk(tagId)
        const design = {};

        for (let designItem of designs) {
            design[designItem.key] = designItem.value;
        }

        campaignsForResponse.push({
            id,
            name,
            text,
            startDate,
            endDate,
            tag,
            user,
            design
        })

    }

    res.setHeader('X-total-count', count);
    res.send(campaignsForResponse);

};

exports.findById = async (req, res) => {
    const reqId = +req.params.id;
    const result = await findByIdInDatabase(reqId);
    res.send(result);
};

exports.update = async (req, res) => {
    const id = +req.params.id;
    const campaign = req.body;
    const {userId, name, tag, text, startDate, endDate, design, screen} = campaign;
    const tagId = await findTagByName(tag);
    let options = await screenParser(screen);
    const screens = await Screen.findAll({
        attributes: ['id'],
        where: options
    }).map(screen => {
        return {campaignId: id, screenId: screen.id}
    });
    await screenCampaign.destroy({where: {campaignId: id}});
    await screenCampaign.bulkCreate(screens);

    await Campaign.update(
        {
            userId,
            name,
            tagId,
            text,
            startDate,
            endDate
        }, {
            where: {id}
        }
    )
        .then(num => {
            // console.log(num)
            // if (num == 1) {
            //     // const result = await findByIdInDatabase(id);
            //     // res.send('Campaign was updated successfully');
            // } else {
            // // no fields for update
            //     res.send({
            //         message: `Cannot update Campaign with id=. Maybe Campaign was not found!`
            //     });
            // }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not update Campaign with id="
            });
        });

    await Design.destroy({
        where: {campaignId: id}
    })
        .then(num => {
            if (num == 1) {
                console.log('Designs is deleted successfully')
            } else {
                console.log(`Cannot delete Design with campaignIdd=${id}. Maybe Design was not found!`)
            }
        });

    await createDesignsFromObject(design, id);

    const result = await findByIdInDatabase(id);

    res.send(result);

};

exports.delete = (req, res) => {
    const ids = req.body;

    Campaign.destroy({
        where: {id: ids}
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Campaign was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Campaign with id=. Maybe Campaign was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Campaign with id="
            });
        });
};

exports.findAllByScreenId = async (req, res) => {
    const screenInf = req.user;
    const screen = await Screen.findOne({attributes: ['id'], where: screenInf}).
    catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while retrieving tutorials."
        });
    });
    const campaigns = await screen.getCampaigns();
    if (!campaigns.length) {
        res.status(404).send('No campaigns found');
    } else {
        const campaignsForResponse = [];
        for (let campaign of campaigns) {
            const {text, tagId} = campaign;
            const designs = await campaign.getDesigns();
            const tag = await Tag.findByPk(tagId);
            const design = {};

            for (let designItem of designs) {
                design[designItem.key] = designItem.value;
            }

            if (design.designType === 'white background') {
                design.background = 'white';
                delete design.designType;
            } else if (design.designType === 'solid color') {
                design.background = design.backgroundColor;
                delete design.designType;
                delete design.backgroundColor;
            } else if (design.designType === 'gradient') {
                design.background = `linear-gradient(to bottom right, ${design.leftColor}, ${design.rightColor})`;
                delete design.rightColor;
                delete design.leftColor;
                delete design.designType;
            } else if (design.designType === 'image') {
                // background = `url('${this.data.design.image}')`
            }

            campaignsForResponse.push({
                text,
                tag: tag.name,
                design
            });
        }
        res.send(campaignsForResponse);
    }
}



const findByIdInDatabase = async reqId => {
    const campaign = await Campaign.findByPk(reqId, {
        include: {
            model: Screen
        }
    })
    let {id, name, text, startDate, endDate, tagId, userId, screens} = campaign;
    screens = screens.map(screen => {
        const {office, floor, room, name} = screen;
        return {office, floor, room, name}
    });
    const designs = await campaign.getDesigns();
    const tag = await Tag.findByPk(tagId, {attributes: ['name']});
    let designForResponse = {};

    const user = await User.findByPk(userId);
    for (let designItem of designs) {
        designForResponse[designItem.key] = designItem.value;

    }
    return {
        id,
        name,
        text,
        startDate,
        endDate,
        tag: tag.name,
        user,
        design: designForResponse,
        screens
    };
}
const createDesignsFromObject = async (design, campaignId) => {
    const designs = Object
        .entries(design)
        .map(entry =>
            ({key: [entry[0]], value: entry[1], campaignId: campaignId})
        );

    await Design.bulkCreate(designs, {returning: true})
        .then(data => console.log('design create ' + JSON.stringify(data))).catch(err => {
            console.log(err);
        });
}
const findTagByName = async name => {
    const {id} = await Tag.findOne({
        attributes: ['id'],
        where: {name}
    });

    return id;
}
const screenParser = async screens => {
    let {office, floor, rooms} = screens;
    if (office === 'ALL') {
        return {office: [1, 2, 3]};
    } else if (floor === 'ALL') {
        let floor;
        await Screen.findAll({attributes: ['floor'], where: {office}}).then(floorsArray => {
            floor = floorsArray.map(floorItem => {
                return floorItem.floor;
            });
        });
        return {office, floor}
    } else if (rooms === 'ALL') {
        let room;
        await Screen.findAll({attributes: ['room'], where: {office, floor}}).then(roomsArray => {
            room = roomsArray.map(roomItem => {
                return roomItem.room;
            });
        });
        return {office, floor, room}
    } else
        return {office, floor, room: rooms}
}


