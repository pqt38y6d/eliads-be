module.exports = (sequelize, Sequelize) => {
    const Screen = sequelize.define("screen", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        office: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        floor: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        room: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        }
    });

    return Screen;
};
