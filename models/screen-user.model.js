module.exports = (sequelize, Sequelize) => {
    const ScreenUser = sequelize.define("screen-user", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        }
    });

    return ScreenUser;
};
