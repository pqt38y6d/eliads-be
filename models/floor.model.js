module.exports = (sequelize, Sequelize) => {
    const Floor = sequelize.define("floor", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        number: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

    return Floor;
};
