module.exports = (sequelize, Sequelize) => {
    const ScreenCampaign = sequelize.define("screen-campaign", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        }
    });

    return ScreenCampaign;
};
