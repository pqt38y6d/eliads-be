const dbConfig = require("../DB/db.config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,
    define: dbConfig.define,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.tags = require("./tag.model.js")(sequelize, Sequelize);
db.screens = require("./screen.model.js")(sequelize, Sequelize);
db.offices = require("./office.model.js")(sequelize, Sequelize);
db.designs = require("./design.model.js")(sequelize, Sequelize);
db.campaigns = require("./campaign.model")(sequelize, Sequelize);
db.screenCampaign = require("./screen-campaign.model")(sequelize, Sequelize);
db.screenUser = require("./screen-user.model")(sequelize, Sequelize);
db.users = require("./user.model")(sequelize, Sequelize);
db.floors = require("./floor.model")(sequelize, Sequelize);


db.campaigns.hasMany(db.designs);
db.designs.belongsTo(db.campaigns);

db.campaigns.belongsToMany(db.screens, {through: db.screenCampaign});
db.screens.belongsToMany(db.campaigns, {through: db.screenCampaign});

db.users.belongsToMany(db.screens, {through: db.screenUser});
db.screens.belongsToMany(db.users, {through: db.screenUser});

db.users.hasMany(db.campaigns);
db.campaigns.belongsTo(db.users);

db.tags.hasMany(db.campaigns);
db.campaigns.belongsTo(db.tags);



module.exports = db;
