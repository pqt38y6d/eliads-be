const express = require("express");
const bodyParser = require("body-parser");
const db = require("./models");
const User = db.users;
const Screen = db.screens;
const passport = require("passport");

const app = express();

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
require("./passport-config")(passport);
require("./routes/tag.routes")(app, passport);
require("./routes/screen.routes")(app, passport);
require("./routes/campaign.routes")(app, passport);
require("./routes/user.routes")(app, passport);
require("./routes/auth.routes")(app, passport);
require("./routes/campaignsForScreen.routes")(app, passport);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});


db.sequelize.sync({force: true});



