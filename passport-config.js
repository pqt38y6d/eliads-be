const Strategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const db = require("./models");
const User = db.users;
const Screen = db.screens;
const secret = 'my secret'


const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret
};

module.exports = passport => {
    passport.use('screenJWT',
        new Strategy(opts, (payload, done) => {
            const { office, floor, room } = payload;
            Screen.findOne({where: {office, floor, room}})
                .then(screen => {
                    if (screen) {
                        return done(null, {office, floor, room});
                    }
                    return done(null, false);
                }).catch(err => console.error(err));
        })
    );

    passport.use('adminJWT',
        new Strategy(opts, (payload, done) => {
            User.findOne({where: {id: payload.id}})
                .then(user => {
                    if (user) {
                        return done(null, {
                            id: user.id,
                            name: user.name,
                            role: user.role,
                        });
                    }
                    return done(null, false);
                }).catch(err => console.error(err));
        })
    );
};
